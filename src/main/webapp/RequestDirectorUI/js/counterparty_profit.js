/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function counterpartyProfit(counterparty_name) {
    // width, height, margins and padding
    var margin = {top: 20, right: 30, bottom: 40, left: 30},
            width = 400 - margin.left - margin.right,
            height = 250 - margin.top - margin.bottom;
    
    // scales
    var xScale = d3.scaleLinear()
            .range([0, width]);

    var yScale = d3.scaleBand()
            .rangeRound([0, height])
            .paddingInner(0.1);

    
//    "Lewis","counterparty_status":"A"},{"counterparty_id":702,"counterparty_name":"Selvyn","counterparty_status":"A"},{"counterparty_id":703,"counterparty_name":"Richard","counterparty_status":"A"},{"counterparty_id":704,"counterparty_name":"Lina","counterparty_status":"A"},{"counterparty_id":705,"counterparty_name":"John","counterparty_status":"A"},{"counterparty_id":706,"counterparty_name":"Nidia
    
    d3.json("http://localhost:8080/frontend/rest/actions/getAllCounterparty/").then(function(data) {
        console.log(data);
        
        d3.select("#counterparty_list").selectAll("option")
                .data(data)
                .data(data)
                .enter()
                .append("li")
                .attr("href", "#")
                .text(function(d) { return d.counterparty_name; } )
                .on("click", function(d) {
                    
                    updateData(d.counterparty_name);
                });
    });

    function updateData(name) {
        $("div#topgraph").empty();
        
        // load data
        d3.json("http://localhost:8080/frontend/rest/actions/getProfForCounterRealize/"+ name).then(function(data) {
            // domains
            xScale.domain(d3.extent(data, function(d) { return d.instrument_count; })).nice();
            yScale.domain(data.map(function(d) { return d.instrument_name; }));

            var xAxis = d3.axisBottom()
                    .scale(xScale);

            // create svg
            var svg = d3.select("div#topgraph")
                    .append("svg")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // create  bars
            svg.selectAll(".bar")
                    .data(data)
                    .enter()
                    .append("rect")
                            .attr("class", function(d) { return "bar bar--" + (d.instrument_count < 0 ? "negative" : "positive"); })
                            .attr("fill", function(d){ return d.instrument_count < 0 ? "#d7191c": "#1a9641"; })
                            .attr("x", function(d) { return xScale(Math.min(0, d.instrument_count)); })
                            .attr("y", function(d) { return yScale(d.instrument_name); })
                            .attr("width", function(d) { return Math.abs(xScale(d.instrument_count) - xScale(0)); })
                            .attr("height", yScale.bandwidth());

            svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")  
                    .call(xAxis);					 

            // add tickNegative
            var tickNeg = svg.append("g")
                            .attr("class", "y axis")
                            .attr("transform", "translate(" + xScale(0) + ",0)")
                            .call(d3.axisLeft(yScale))
                    .selectAll(".tick")
                    .filter(function(d, i) { return data[i].instrument_count < 0; });

            tickNeg.select("line")
                    .attr("x2", 6);

            tickNeg.select("text")
                    .attr("x", 9)
                    .style("text-anchor", "start");		

        });
    }
    
    updateData(counterparty_name);
}
