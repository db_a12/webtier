/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var defaultPath = "rest/actions";

function most_sold_table(id)
{
    $.ajax({
        type: "GET",
        url: defaultPath + "/getMost/sell/" + id,
        dataType: "json",
        success:function(data){
       
           // $("#leftscroller").append("<table id=\"sellcount\"class=\"table table-striped table-bordered\"></table>");
            $("#sellcount").append("<thead><tr><th>Name</th><th>Count</th></tr></thead>");
            $("#sellcount").append("<tbody>");
           // var table = $('#sellcount').DataTable();
           // table.rows.add(data);
            $.each(data, function(i){
               // table.rows.add(data[i]);
                $("#sellcount").append("<tr><td>"+ data[i]["instrument_name"] + "</td><td>" + data[i]["instrument_count"] + "</td></tr>");
            });
            $("#sellcount").append("</tbody>"); 
            $("#sellcount").DataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false,
            "searching":false,
            "info":false,
            "aaSorting": []
            
            
        });
        },
        error: function(data)
        {
            alert("failed");
        }
    });
}
function most_bought_table(id)
{
    $.ajax({
        type: "GET",
        url: defaultPath + "/getMost/buy/" + id,
        dataType: "json",
        success:function(data){
       
           // $("#leftscroller").append("<table id=\"sellcount\"class=\"table table-striped table-bordered\"></table>");
            $("#buycount").append("<thead><tr><th>Name</th><th>Count</th></tr></thead>");
            $("#buycount").append("<tbody>");
           // var table = $('#sellcount').DataTable();
           // table.rows.add(data);
            $.each(data, function(i){
               // table.rows.add(data[i]);
                $("#buycount").append("<tr><td>"+ data[i]["instrument_name"] + "</td><td>" + data[i]["instrument_count"] + "</td></tr>");
            });
            $("#buycount").append("</tbody>"); 
            $("#buycount").DataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false,
            "searching":false,
            "info":false
            
            
        });
        },
        error: function(data)
        {
            alert("failed");
        }
    });
}

function recent_trade_table(id)
{
    $.ajax({
        type: "GET",
        url: defaultPath + "/getCounterDeals/" + id,
        dataType: "json",
        success:function(data){
       
           // $("#leftscroller").append("<table id=\"sellcount\"class=\"table table-striped table-bordered\"></table>");
            $("#recenttrades").append("<thead><tr><th>Deal Type</th><th>Instrument Name</th><th>Deal Amount</th><th>Deal Quantity</th></tr></thead>");
            $("#recenttrades").append("<tbody>");
           // var table = $('#sellcount').DataTable();
           // table.rows.add(data);
            $.each(data, function(i){
               // table.rows.add(data[i]);
                $("#recenttrades").append("<tr><td>"+ data[i]["deal_type"] + "</td><td>" + data[i]["instrument_name"] + "</td><td>"+ data[i]["deal_amount"] + "</td><td>"+ data[i]["deal_quantity"] + "</td></tr>");
            });
            $("#recenttrades").append("</tbody>"); 
            $("#recenttrades").DataTable({
            "scrollY": "70vh",
            "scrollCollapse": true,
            "paging": false,
            "searching":false,
            "info":false
            
            
        });
        },
        error: function(data)
        {
            alert("failed");
        }
    });
}

function line_table(){

    var st = document.getElementById("sidebarToggle");
    st.style.display = "block";
    
    $("#input-form").empty();
    
    d3.json("http://localhost:8080/frontend/rest/actions/getAllDeal").then(function(xhr) {
//        $(".input-form").
        // create the table header
        var thead = d3.select("#input-form")
                .append("table").selectAll("th")
                .data(d3.keys(xhr[0]))
                .enter().append("th").text(function(d){return d;});
        
        // fill the table
        // create rows
        var tr = d3.select("table").selectAll("tr")
                .data(xhr).enter().append("tr");
        // cells
        var td = tr.selectAll("td")
                .data(function(d){return d3.values(d);})
                .enter().append("td")
                .text(function(d) {return d;});
    });

}