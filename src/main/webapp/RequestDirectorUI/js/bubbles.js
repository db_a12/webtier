/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var defaultPath = "rest/actions";

$("#instrument-profitability").on("click", function (event) {
    // alert("dog");
    instrument_bubble_populate();
    event.preventDefault();
});

$("#stakeholder-profitability").on("click", function (event) {
   //   alert("dog");
    stakeholder_bubble_populate();
    event.preventDefault();
});
function instrument_bubble_populate()
{
    $.ajax({
        type: "GET",
        url: defaultPath + '/getInstrumentProfits',
        dataType: "json",
        success: function (data)
        {
          //  alert("on the move");
            bubble_chart(data);
        },
        error: function (response)
        {
            alert(response);
        }
    });
}
function stakeholder_bubble_populate()
{
    $.ajax({
        type: "GET",
        url: defaultPath + '/getStakeholderProfits',
        dataType: "json",
        success: function (data)
        {
          //  alert("on the move");
            bubble_chart(data);
        },
        error: function (response)
        {
            alert(response);
        }
    });
}


function bubble_chart(data)
{
    $("#input-form").empty();
    dataset = {"children":data};
    console.log(dataset);

    var diameter = 600;
    var color = d3.scaleOrdinal(d3.schemeCategory20);

    var bubble = d3.pack(dataset)
            .size([diameter, diameter])
            .padding(1.5);

    var svg = d3.select("#input-form")
            .append("svg")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubble");

    var nodes = d3.hierarchy(dataset)
            .sum(function (d) {
                return Math.abs(d.instrument_count);
            });

    var node = svg.selectAll(".node")
            .data(bubble(nodes).descendants())
            .enter()
            .filter(function (d) {
                return  !d.children
            })
            .append("g")
            .attr("class", "node")
            .attr("transform", function (d) {
                return "translate(" + d.x + "," + d.y + ")";
            });

    node.append("title")
            .text(function (d) {
                return d.instrument_name + ": " + d.instrument_count;
            });

    node.append("circle")
            .attr("r", function (d) {
                return d.r;
            })
            .style("fill", function (d, i) {
                return d.data.instrument_count <= 0 ? "red" : "green";
            });

    node.append("text")
            .attr("dy", ".2em")
            .style("text-anchor", "middle")
            .text(function (d) {
                return d.data.instrument_name.substring(0, d.r / 3);
            })
            .attr("font-family", "sans-serif")
            .attr("font-size", function (d) {
                return d.r / 5;
            })
            .attr("fill", "white");

    node.append("text")
            .attr("dy", "1.3em")
            .style("text-anchor", "middle")
            .text(function (d) {
                return d.data.instrument_count;
            })
            .attr("font-family", "Gill Sans", "Gill Sans MT")
            .attr("font-size", function (d) {
                return d.r / 5;
            })
            .attr("fill", "white");

    d3.select(self.frameElement)
            .style("height", diameter + "px");
}

