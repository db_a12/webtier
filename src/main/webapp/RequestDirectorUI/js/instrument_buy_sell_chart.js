/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$("#instrument-buysell").on("click", function (event) {
    showInstrumentBuySellChart();
    event.preventDefault();
});

function showInstrumentBuySellChart() {
    var st = document.getElementById("sidebarToggle");
    st.style.display = "block";
    
    $("#input-form").empty();
    $("#input-form").height("100%");
    
    var instrument_select = d3.select("#input-form")
            .append("select")
            .attr("id", "inds");
    
    d3.json("http://localhost:8080/frontend/rest/actions/getAllInstrument/").then(function(json) {
        instrument_select.selectAll("option")
                .data(json)
                .enter()
                .append("option")
                .attr("value", function(d) { return d.instrument_id; })
                .text(function(d) { return d.instrument_name; });
        
        // Start with first instrument
        setInstrument(json[0].instrument_id);
    });
    
    // Set the dimensions of the canvas / graph
    var margin = {top: 10, right: 50, bottom: 200, left: 50},
        width = $("#input-form").width() - margin.left - margin.right,
        height = $("#input-form").height() - margin.top - margin.bottom;
    
    // Set the ranges
    var x = d3.scaleTime().rangeRound([0, width]);
    var y = d3.scaleLinear().rangeRound([height, 0]);

    // Define the line
    var stateline = d3.line()
                    .curve(d3.curveStepBefore)
        .x(function(d) { return x(d.time); })
        .y(function(d) { return y(d.price); });

    // Adds the svg canvas
    var svg = d3.select("#input-form")
            .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("transform",
                      "translate(" + margin.left + "," + margin.top + ")");

    var data = {
        buy_data: [],
        sell_data: []
    };

    d3.select('#inds')
            .on("change", function() {
                var sect = document.getElementById("inds");
                var section = sect.options[sect.selectedIndex].value;
                
                setInstrument(section);
            });

    function setInstrument(instrument_id) {
        console.log("Loading data for instrument " + instrument_id);
        d3.json("http://localhost:8080/frontend/rest/actions/getBuySellID/" + instrument_id).then(function(json) {
            // Clear current data
            data.buy_data = [];
            data.sell_data = [];

            json.forEach(function(d) {
                d.time = new Date(d.time);//parseTime(d.time);
                d.price = +d.price;

                if(d.type === "S") {
                    data.sell_data.push(d);
                } else {
                    data.buy_data.push(d);
                }
            });

            updateGraph(data);
        });
    }

    function updateGraph(data) {
        // Scale the range of the data
        x.domain([
            d3.min([
                d3.min(data.buy_data, function(d) {return d.time;}),
                d3.min(data.sell_data, function(d) {return d.time;})]),
            d3.max([
                d3.max(data.buy_data, function(d) {return d.time;}),
                d3.max(data.sell_data, function(d) {return d.time;})])
        ]);
        y.domain([
            d3.min([
                d3.min(data.buy_data, function(d) {return d.price;}),
                d3.min(data.sell_data, function(d) {return d.price;})]),
            d3.max([
                d3.max(data.buy_data, function(d) {return d.price;}),
                d3.max(data.sell_data, function(d) {return d.price;})])
        ]);
        
        // Remove all lines
	svg.selectAll("path").remove();
        
        // Add buy line
        svg.append("path")
                .attr("class", "buy_line")
                .datum(data.buy_data)
                .attr("fill", "none")
                .attr("stroke", "steelblue")
                .attr("stroke-linejoin", "round")
                .attr("stroke-linecap", "round")
                .attr("stroke-width", 1.5)
                .attr("d", stateline);
        
        svg.append("path")
                .attr("class", "sell_line")
                .datum(data.sell_data)
                .attr("fill", "none")
                .attr("stroke", "green")
                .attr("stroke-linejoin", "round")
                .attr("stroke-linecap", "round")
                .attr("stroke-width", 1.5)
                .attr("d", stateline);
        
        d3.select(".xaxis")
                .call(d3.axisBottom(x));
        
        d3.select(".yaxis")
                .call(d3.axisLeft(y));
    }
    
      
    // Add the X Axis
    svg.append("g")
            .attr("class", "xaxis")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

    // Add the Y Axis
    svg.append("g")
            .attr("class", "yaxis")
            .call(d3.axisLeft(y))
        .append("text")
            .attr("fill", "#000")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .text("Price ($)");
};
