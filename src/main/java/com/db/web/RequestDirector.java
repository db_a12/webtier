/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.web;

import com.db.CounterDeals;
import com.db.CounterParty;
import com.db.Deal;
import com.db.InstrumentCount;
import com.db.TimeBuySell;
import com.db.core.LoginDAO;
import com.db.core.TableDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Graduate
 */
@Path("/actions")
public class RequestDirector implements IRequestDirector {

    @GET
    @Path("/testing")
    @Produces(MediaType.TEXT_HTML)
    public Response urlTest() {
        System.out.println("went here");
        String res = "<html> <title> hanc </title> <body><h1> the redirect is working.. </h1></body></html> ";
        return Response.status(200).entity(res).build();
    }

    @GET
    @Path("/sayhello")
    @Produces(MediaType.TEXT_HTML)
    public Response sayHtmlHelloTest() {

        String res = "<html> <title> hanc </title> <body><h1> the redirect is working.. </h1></body></html> ";
        return Response.status(200).entity(res).build();
    }

    @POST
    @Path("/login")
    @Produces(MediaType.TEXT_HTML)
    public Response login(@FormParam("username") String user, @FormParam("password") String password) throws SQLException {
        System.out.println("called login");
        LoginDAO loginDAO = new LoginDAO();
        if(loginDAO.validUser(user, password))
        {
            return Response.status(200).entity("Successful").build();
        }
        else
        {
            return Response.status(401).entity("Invalid User/Password").build();
        }
      //  String result = "<html> " + "<title>" + "hanc" + "</title>"
      //          + "<body><h1>" + "Your login result is:" + "</h1>" + loginDAO.validUser(user, password) + "<h1></h1</body>" + "</html> ";
        
        
        //return Response.status(200).entity(result).build();
    }

    @GET
    @Path("/getAllCounterparty")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCounterparty() {

        TableDAO tableDAO = new TableDAO();
        ArrayList<CounterParty> parties = tableDAO.getAllCounterParty();
        if (parties == null) {
            return Response.ok("getAllCounterparty returned null when getting collection.", MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.ok(parties, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/getAllDeal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDeal() {
        TableDAO tableDAO = new TableDAO();
        ArrayList<Deal> deals = tableDAO.getAllDeals();
        if (deals == null) {
            return Response.ok("getAllDeal returned null when getting collection.", MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.ok(deals, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/getAllInstrument")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllInstrument() {
        TableDAO tableDao = new TableDAO();
        return Response.ok(tableDao.getAllInstrument(), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/getInstrumentInfo/{option}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInstrumentInfo(@PathParam("option") String option, @PathParam("id") String id) {
        TableDAO dao = new TableDAO();
        if (option.toLowerCase().equals("buy")) {
            return Response.ok(dao.getBuy(Integer.parseInt(id)), MediaType.APPLICATION_JSON_TYPE).build();
        } else if (option.toLowerCase().equals("sell")) {
            return Response.ok(dao.getSell(Integer.parseInt(id)), MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            return Response.ok("You Screwed up get intrument info request", MediaType.APPLICATION_JSON_TYPE).build();
        }

    }
    
    @GET
    @Path("/getCounterDeals/{option}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCounterDeals(@PathParam("option") String option) {
        TableDAO dao = new TableDAO();
        ArrayList<CounterDeals> countpDeals = dao.getCounterDeals(option);
        if(countpDeals == null)
        {
            
            return Response.ok("You Screwed up get intrument info request", MediaType.APPLICATION_JSON_TYPE).build();
        }
        else
        {
            return Response.ok(countpDeals, MediaType.APPLICATION_JSON).build();
        }

    }
    
    // insert the counterparty name
    @GET
    @Path("/getBuySellID/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBuySellID(@PathParam("id") String id)
    {
        TableDAO dao = new TableDAO();
        ArrayList<TimeBuySell> bs = dao.getBuySell(Integer.parseInt(id));
        if(bs != null)
        {
            return Response.ok(bs, MediaType.APPLICATION_JSON).build();
        }
        return Response.status(404).entity("not found").build();
    }
    @GET
    @Path("/getMost/{option}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMost(@PathParam("option") String option, @PathParam("id")String id)
    {
        TableDAO dao = new TableDAO();
          if (option.toLowerCase().equals("buy")) {
            return Response.ok(dao.countBuySell("B", id), MediaType.APPLICATION_JSON_TYPE).build();
        } else if (option.toLowerCase().equals("sell")) {
            return Response.ok(dao.countBuySell("S", id), MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            return Response.ok("You Screwed up getting instrument count", MediaType.APPLICATION_JSON_TYPE).build();
        }
    }
    
    @GET
    @Path("/getProfForCounterRealize/{option}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfForCounterRealize(@PathParam("option") String option)
    {
        TableDAO dao = new TableDAO();
        return Response.ok(dao.getProfForCountRealize(option), MediaType.APPLICATION_JSON).build();
    }
    
    @GET
    @Path("/getCounterProfits")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCounterProfits()
    {
        return null;
    }
    
    @GET
    @Path("/getInstrumentProfits")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInstrumentProfits()
    {
        TableDAO dao = new TableDAO();
        ArrayList<InstrumentCount> instC = dao.getCounterProfits();
        if(instC != null)
        {
            return Response.ok(instC, MediaType.APPLICATION_JSON).build();
        }
        else
        {
             return Response.status(404).entity("not found").build();
        }
    }
    @GET
    @Path("/getStakeholderProfits")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStakeholderProfits()
    {
        TableDAO dao = new TableDAO();
        ArrayList<InstrumentCount> instC = dao.getStakeholderProfits();
        if(instC != null)
        {
            return Response.ok(instC, MediaType.APPLICATION_JSON).build();
        }
        else
        {
             return Response.status(404).entity("not found").build();
        }
    }
}
