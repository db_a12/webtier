/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.web;

import java.sql.SQLException;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author Graduate
 */
public interface IRequestDirector {
    
    public Response urlTest();
    
    public Response sayHtmlHelloTest();
    
    public Response login(String user, String password)  throws SQLException ;
        
    
    
}
