<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="theQuery" class="com.db.core.LoginDAO" scope="application"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="RequestDirectorUI/css/login.css"/>
        <link type="text/css" rel="stylesheet" href="RequestDirectorUI/css/graphs.css"/>
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
        <link rel="icon" href="RequestDirectorUI/resources/dblogo.png">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="http://d3js.org/d3.v3.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.5.0/d3.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

        <title>DB Investment Service</title>
    </head>
    <header id="header"></header>
    <body>
        <div class="center-page">
            <div class="inner-center">
                <div id="sidebar">
                    <ul id="sidebar-nav">
                        <!--                        <li id="sidebar-brand">
                                                    <a href="#">Investment Service</a>
                                                </li>-->
                        <li> <a id="counterparty-dashboard">Counterparties</a> </li>
                        <li ><a id="instrument-profitability" href="#">Instrument Profitability</a> </li>
                        <li> <a id="stakeholder-profitability"href="#">Stakeholder Profitability</a> </li>
                        <li> <a id="instrument-buysell"href="#">Buy Sell Activity</a> </li>
                    </ul>
                </div>
                <div id="sidebarToggle">
                    <span> &#9776 </span>
                </div>
                <!--Form-->
                <div id="input-form">
                    <form id="loginForm">
                        <br>
                        <input type="text" placeholder="Username" name="username"/>
                        <br>
                        <input type ="password" placeholder="Password" name="password"/>
                        <br>
                        <button id="loginButton" type="submit">Login</button>   
                    </form>

                    <p id ="suceeded"> </p>
                </div>

            </div>
        </div>
    </body>
    <script src="RequestDirectorUI/js/main.js"></script>
    <script src="RequestDirectorUI/js/table.js"></script>
    <script src="RequestDirectorUI/js/bubbles.js"></script>
    <script src="RequestDirectorUI/js/instrument_buy_sell_chart.js"></script>
    <script src="RequestDirectorUI/js/dashboard.js"></script>
    <script>
        $(function () {
            $("#header").load("Templates/header.jsp");
            $("#footer").load("Templates/footer.jsp");
        });
    </script>
</html>
