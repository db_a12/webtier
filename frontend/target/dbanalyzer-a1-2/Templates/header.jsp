<%-- 
    Document   : header
    Created on : 08-Aug-2018, 13:37:59
    Author     : Graduate
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <script src="RequestDirectorUI/js/main.js"></script>
        <title>Header</title>
    </head>
    <header>
        <div class="main-header use-identifier" id="top">
            
            <nav class="cross-navigation trackMe" id="cross-navigation">
                <div class="cross-navigation-ul-wrapper">
                    <ul id="nav-cross" class="lv1">
                        <li class="first">
                            <a href="/frontend" class="nav_crossb" title="Home" id="nav-cross_home">Home</a>
                            <a href="/frontend" class="nav_crossb" title="Login" id="nav-cross_login">Login</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="identifier-and-logo">
                <a href="/frontend" class="identifier">
                    <img src="RequestDirectorUI/resources/dbword.png" alt="Deutsche Bank">
                </a>
                <a href="/frontend" class="logo">
                    <img src="RequestDirectorUI/resources/dblogo.png" alt="Deutsche Bank Logo">
                </a>
            </div>

            <div style="clear:both"></div>
        </div>
    </header>
</html>
